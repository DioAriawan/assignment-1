import  React,{Component} from 'react';
import {StyleSheet,Text,View,TextInput,TouchableOpacity,Image, } from 'react-native';
import background from '../src/asset/images/background.jpg';
import logo from '../src/asset/images/foto.png';
import Icon from 'react-native-vector-icons/AntDesign';

export default class SignUp extends Component{
  constructor() {
    super();
    this.state = {showPassword:true};
}
show=()=>{
  this.setState({
    showPassword: !this.state.showPassword
  })

}
buttonPress= ()=>{
  this.setState({
      username:'',
      email:'',
      password:'',
      phone:''
      
  })
}
  render() {
    const {showPassword} = this.state
    return (
      <View style={styles.container}>
        <Image style={styles.background} source={background}/>
        <Image style={styles.logo} source={logo}/>
        <Text style={{fontSize:25,fontWeight:'bold'}}>Please SignUp First!</Text> 
        <View style={styles.input}>
            <Icon name='user' size={30} color='black'/>
            <TextInput value={this.state.username} placeholder="Username" onChangeText ={(typing)=>this.setState({username:typing})}/>
        </View>
        <View style={styles.input}>
            <Icon name='mail' size={30} color='black'/>
            <TextInput value={this.state.email} placeholder='Email' onChangeText ={(typing)=>this.setState({email:typing})}/>
        </View>
        <View style={styles.input}>
          <Icon name="eyeo" size={30} color="black" />
          <TextInput value={this.state.password} secureTextEntry={showPassword} placeholder="Password" style={{flex: 1}} onChangeText ={(typing)=>this.setState({password:typing})} />
          <TouchableOpacity onPress={this.show}>
            <Icon name ="eye" size={30} color="black" />
          </TouchableOpacity>
        </View>
        <View style={styles.input}>
            <Icon  name='phone' size={30} color='black'/>
            <TextInput value={this.state.phone} placeholder="Phone Number" onChangeText ={(typing)=>this.setState({phone:typing})}/>
        </View>
        <TouchableOpacity onPress={this.buttonPress} style={{borderWidth:2,borderColor:'black',width:300,alignItems:'center',backgroundColor:'#6495ED',marginTop:10,borderRadius:10,}}>
            <Text style={{color:'white',fontWeight:'bold',fontSize:20,}}>SignUp</Text>
          </TouchableOpacity>
          <View style={{flexDirection:'row',alignItems:'center'}}>
            <Text style={{fontSize:20,color:'black',fontWeight:'600',}}>Already SignUp?</Text>
              <TouchableOpacity>
                 <Text style={{color:'blue',fontSize:20,}} title="Go Login"
                    onPress={() => this.props.navigation.navigate('Login')}>Login </Text> 
              </TouchableOpacity>

          </View>
          
        
        </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex:1,
    justifyContent:'center',
    alignItems: 'center',
    backgroundColor: "#F0FFFF"
  },
  input:{
    flexDirection: 'row',
    alignItems: 'center',
    width: 300,
    borderWidth:3,
    backgroundColor:'white',
    borderRadius:20,
    marginTop:20,
    paddingHorizontal:8


  },
  logo:{
    width:200,
    height:150,
    

},
  background:{
  flex: 1,
  resizeMode: 'cover',
  justifyContent: 'center',
  alignItems: 'center',
  position: 'absolute',
  width: '100%',
  height: '100%',

},


})