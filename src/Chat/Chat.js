import React, { Component } from 'react'
import { Text, View ,Image,} from 'react-native'
import Icon from 'react-native-vector-icons/AntDesign';
import Profil from '../asset/images/Profil.jpg'


export class Chat extends Component {
    render() {
        return (
            <View style={{flex:1,backgroundColor: "#F0FFFF",}}>
                <View style={{backgroundColor:'#B6B6B4',height:70,width:400,flexDirection:'row',alignItems:'center',}}>
                    <Icon name='left' size={30}/>
                    <Image style={{ width: '15%', borderRadius: 100, height: 60,borderWidth:2,marginRight:8}} source={Profil}></Image>
                    <Text style={{fontSize:25,fontWeight:'bold',flex:1}}>Dio Ariawan</Text>
                <View style={{flexDirection:'row',width:120,justifyContent:'space-evenly'}}>
                    <Icon name='phone' size={30}/>
                    <Icon name='videocamera' size={30}/>
                    </View>
                </View>
                <View style={{flexDirection:'column',marginTop:20,alignItems:'flex-end'}}>
                    <View style={{ backgroundColor: '#808080', width: '40%', height: 60, marginTop:20,borderRadius:10,alignItems:'center',justifyContent:'center'}}>
                        <Text style={{fontSize:25,fontWeight:'bold'}}>Heii!!</Text>
                    </View>
                </View>
                <View style={{flexDirection:'column',marginTop:20,alignItems:'flex-start',height:100}}>
                    <View style={{ backgroundColor: '#808080', width: '40%', height: 60, marginTop:20,borderRadius:10,alignItems:'center',justifyContent:'center'}}>
                    <Text style={{fontSize:25,fontWeight:'bold'}}>Sup?</Text>
                    </View>
                </View>
            </View>
        )
    }
}

export default Chat
