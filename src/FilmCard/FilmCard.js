import React, { Component } from 'react'
import { Text, View ,Image} from 'react-native'

export class FilmCard extends Component {
    render() {
        return (
            <View style={{ backgroundColor: '#34282C', paddingHorizontal: '2%', height: '100%' }}>
                <View style={{ alignItems: 'center', marginVertical: 20 }}>
                    <Text style={{ color: 'white',fontSize:20 }}>Makoto Hinkai Movies</Text>
                </View>
                <View style={{backgroundColor:'#3B3131',width:350,height:200,borderRadius:20,padding:10,flexDirection:'row',justifyContent:'space-around',alignSelf:'center',}}>
                    <Image style={{height:'100%',width:'35%',borderRadius:10,}}source={{uri:'https://i.ibb.co/prq9Q1f/Kimi-no-Na-wa-Visual.jpg'}}/>
                    <View style={{justifyContent:'space-between',}}>
                        <Text style={{fontSize:25,color:'white'}}>Kimi No Nawa?</Text>
                        <Text style={{fontSize:20,color:'white'}}>Romaji:君の名は</Text>
                        <Text style={{fontSize:20,color:'white'}}>Diretor:Makoto Shinkai</Text>
                    </View>
                </View>
                <View style={{backgroundColor:'#3B3131',width:350,height:200,borderRadius:20,padding:10,flexDirection:'row',justifyContent:'space-around',alignSelf:'center',marginTop:15}}>
                    <Image style={{height:'100%',width:'35%',borderRadius:10}}source={{uri:'https://cinemapolis.org/wp-content/uploads/WeatheringWithYouPoster.jpg'}}/>
                    <View style={{justifyContent:'space-between',}}>
                        <Text style={{fontSize:20,color:'white'}}>Weathering With You</Text>
                        <Text style={{fontSize:20,color:'white'}}>Romaji:天気の子</Text>
                        <Text style={{fontSize:20,color:'white'}}>Diretor:Makoto Shinkai</Text>
                    </View>
                </View>
                <View style={{backgroundColor:'#3B3131',width:350,height:200,borderRadius:20,padding:10,flexDirection:'row',justifyContent:'space-around',alignSelf:'center',marginTop:15}}>
                    <Image style={{height:'100%',width:'35%',borderRadius:10}}source={{uri:'https://m.media-amazon.com/images/M/MV5BMGI0NTNlNGYtOTY2My00MWQ2LThhZjMtNTA3NjM2YTY3NjI5XkEyXkFqcGdeQXVyMTcyMDgxNQ@@._V1_UY1200_CR85,0,630,1200_AL_.jpg'}}/>      
                    <View style={{justifyContent:'space-between'}}>
                        <Text style={{fontSize:20,color:'white'}}>Cross Road</Text>
                        <Text style={{fontSize:20,color:'white'}}>Romaji:クロスロード</Text>
                        <Text style={{fontSize:20,color:'white'}}>Diretor:Makoto Shinkai</Text>
                    </View>
                </View>
            </View>
        )
    }
}

export default FilmCard
