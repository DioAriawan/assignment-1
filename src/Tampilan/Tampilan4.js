import React, { Component } from 'react'
import { Text, View } from 'react-native'

export class Tampilan4 extends Component {
    render() {
        return (
            <View style={{ backgroundColor: '#477eff', paddingHorizontal: '2%', height: '100%' }}>
                <View style={{ alignItems: 'center', marginVertical: 20 }}>
                    <Text style={{ color: 'white' }}>Parent Component</Text>
                </View>
                <View style={{alignItems:'center'}}>
                    <View style={{backgroundColor:'cyan',width:250,height:120,borderWidth:2}}/>
                </View>
                <View style={{flexDirection:'column',alignItems:'center',marginTop:60}}>
                    <View style={{backgroundColor:'green',width:350,height:60,borderWidth:2,marginBottom:30}}/>
                    <View style={{backgroundColor:'green',width:350,height:60,borderWidth:2,marginBottom:30}}/>
                </View>
                <View style={{flexDirection:'column',alignItems:'flex-end'}}>
                <View style={{backgroundColor:'blue',width:150,height:40,borderWidth:2,marginBottom:30,marginRight:15}}/>
                </View>
            </View>
        )
    }
}

export default Tampilan4
