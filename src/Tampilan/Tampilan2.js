import React, { Component } from 'react'
import { Text, View } from 'react-native'

export class Tampilan2 extends Component {
    render() {
        return (
            <View style={{ backgroundColor: '#477eff', paddingHorizontal: '2%', height: '100%' }}>
                <View style={{alignItems:'center',marginVertical:10}}>
                    <Text style={{color:'white'}}>Parent Component</Text>
                </View>
                <View style={{ width: '100%', height: 150, backgroundColor: '#ff477e', padding: '3%', alignItems: 'center', flexDirection: 'row' }}>
                    <View style={{ width: '25%', borderRadius: 100, backgroundColor: 'yellow', height: 100,borderWidth:2 }} />
                    <View style={{ flexDirection: 'column', paddingLeft: '3%' }}>
                        <View style={{ backgroundColor: '#aad576', width: '90%', height: 50, marginBottom: 20,borderWidth:2 }} />
                        <View style={{ flexDirection: 'row',alignItems:'flex-start',}}>
                            <View style={{ backgroundColor: 'red', width: '40%', height: 50,marginRight:10,borderWidth:2 }} />
                            <View style={{ backgroundColor: 'yellow', width: '40%', height: 50,borderWidth:2}} />
                        </View>
                    </View>
                </View>
                <View style={{flexDirection:'column',marginTop:20,alignItems:'flex-start'}}>
                    <View style={{ backgroundColor: '#aad576', width: '90%', height: 50, marginBottom: 20,borderWidth:2 }}/>
                    <View style={{ backgroundColor: '#aad576', width: '90%', height: 50, marginBottom: 20,borderWidth:2 }}/>
                    <View style={{ backgroundColor: '#aad576', width: '90%', height: 50, marginBottom: 20,borderWidth:2 }}/> 
                </View>
                <View style={{flexDirection:'column',marginTop:20,alignItems:'flex-end'}}>
                    <View style={{ backgroundColor: 'green', width: '90%', height: 50, marginBottom: 20,borderWidth:2 }}/>
                    <View style={{ backgroundColor: 'green', width: '90%', height: 50, marginBottom: 20,borderWidth:2 }}/>
                    <View style={{ backgroundColor: 'green', width: '90%', height: 50, marginBottom: 20,borderWidth:2 }}/> 
                </View>
            </View>
        )
    }
}

export default Tampilan2
