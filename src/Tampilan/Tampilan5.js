import React, { Component } from 'react'
import { Text, View } from 'react-native'

export class Tampilan5 extends Component {
    render() {
        return (
            <View style={{ backgroundColor: '#477eff', paddingHorizontal: '2%', height: '100%' }}>
                <View style={{ alignItems: 'center', marginVertical: 20 }}>
                    <Text style={{ color: 'white' }}>Parent Component</Text>
                </View>
                <View style={{alignItems:'center'}}>
                    <View style={{backgroundColor:'cyan',width:350,height:150,borderWidth:2,paddingHorizontal:'4%'}}>
                        <View style={{ alignItems: 'center', marginVertical: 20 }}>
                            <Text style={{color:'black',fontWeight:'bold',}}>Child Component</Text>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'space-between',}}>
                            <View style={{ width: '25%', borderRadius: 100, backgroundColor: 'yellow', height:80,borderWidth:2,alignItems:'center',justifyContent:'center'}}>
                                <Text style={{color:'black',fontWeight:'bold',fontSize:20}}>Child</Text>
                            </View>
                            <View style={{backgroundColor:'green',width:200,height:40,borderWidth:2,marginTop:30,alignItems:'center',justifyContent:'center'}}>
                            <Text style={{color:'black',fontWeight:'bold',fontSize:20}}>Child</Text> 
                            </View>
                        </View>
                    </View>
                </View>
                <View style={{alignItems:'center',marginTop:40}}>
                    <View style={{backgroundColor:'cyan',width:350,height:150,borderWidth:2,paddingHorizontal:'4%'}}>
                        <View style={{ alignItems: 'center', marginVertical: 20 }}>
                            <Text style={{color:'black',fontWeight:'bold',}}>Child Component</Text>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'space-evenly'}}>
                            <View style={{backgroundColor:'green',width:80,height:40,borderWidth:2,alignItems:'center',justifyContent:'center'}}>
                                <Text style={{color:'black',fontWeight:'bold',fontSize:15}}>Child</Text>
                            </View>
                            <View style={{backgroundColor:'green',width:80,height:40,borderWidth:2,alignItems:'center',justifyContent:'center'}}>
                                <Text style={{color:'black',fontWeight:'bold',fontSize:15}}>Child</Text>
                            </View>
                            <View style={{backgroundColor:'green',width:80,height:40,borderWidth:2,alignItems:'center',justifyContent:'center'}}>
                                <Text style={{color:'black',fontWeight:'bold',fontSize:15}}>Child</Text>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={{flexDirection:'column',alignItems:'center',marginTop:20}}>
                    <View style={{backgroundColor:'green',width:300,height:40,borderWidth:2,justifyContent:'center',alignItems:'center'}}>
                        <Text style={{color:'black',fontSize:20,fontWeight:'bold'}}>Child Component</Text>
                    </View>
                    <View style={{backgroundColor:'green',width:300,height:40,borderWidth:2,justifyContent:'center',alignItems:'center',marginTop:20}}>
                        <Text style={{color:'black',fontSize:20,fontWeight:'bold'}}>Child Component</Text>
                    </View>
                </View>
            </View>
        )
    }
}

export default Tampilan5
