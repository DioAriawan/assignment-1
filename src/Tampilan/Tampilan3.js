import React, { Component } from 'react'
import { Text, View } from 'react-native'

export class Tampilan3 extends Component {
    render() {
        return (
            <View style={{ backgroundColor: '#477eff', paddingHorizontal: '2%', height: '100%' }}>
                <View style={{ alignItems: 'center', marginVertical: 20 }}>
                    <Text style={{ color: 'white' }}>Parent Component</Text>
                </View>
                <View style={{ flexDirection: 'row', marginBottom: 30,justifyContent:'space-evenly'}}>
                    <View style={{ backgroundColor: 'cyan', width: '40%', height: 90,borderWidth:2 }} />
                    <View style={{ backgroundColor: 'cyan', width: '40%', height: 90,borderWidth:2 }} />
                </View>
                <View style={{ flexDirection: 'row', marginBottom: 30,justifyContent:'space-evenly'}}>
                    <View style={{ backgroundColor: 'cyan', width: '40%', height: 90,borderWidth:2 }} />
                    <View style={{ backgroundColor: 'cyan', width: '40%', height: 90,borderWidth:2 }} />
                </View>
                <View style={{ flexDirection: 'row', marginBottom: 30,justifyContent:'space-evenly'}}>
                    <View style={{ backgroundColor: 'cyan', width: '40%', height: 90,borderWidth:2 }} />
                    <View style={{ backgroundColor: 'cyan', width: '40%', height: 90,borderWidth:2 }} />
                </View>
                <View style={{ flexDirection: 'row', marginBottom: 30,justifyContent:'space-evenly'}}>
                    <View style={{ backgroundColor: 'cyan', width: '40%', height: 90,borderWidth:2 }} />
                    <View style={{ backgroundColor: 'cyan', width: '40%', height: 90,borderWidth:2 }} />
                </View>
            </View>
        )
    }
}

export default Tampilan3
