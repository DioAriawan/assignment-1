import  React,{Component} from 'react';
import {StyleSheet,Text,View,TextInput,TouchableOpacity,Button,Image, ImageBackground } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

export default class Forget extends Component{
    constructor() {
        super();
        this.state = {
          showPassword:true}
    }
    show=()=>{
      this.setState({
        showPassword: !this.state.showPassword
      })
    
    }
    buttonPress= ()=>{
      this.setState({
          pass:'',
          password:'',
          
      })
    }
    render() {
        const {showPassword} = this.state
        return (
          <View style={styles.container}>
              <Image style={styles.background} source={{uri:'https://i.pinimg.com/474x/ae/09/ab/ae09abaf317f33aa794cc5c8bc7f1249.jpg'}}></Image>
              <View style={styles.text}>
                <Text style={{fontSize:40,fontStyle:'italic',fontWeight:'bold',color:'black'}}>Create New Password</Text>
                <Text style={{fontSize:20,fontStyle:'italic',fontWeight:'bold',color:'black'}}>Your new password must Diffrent from previous used password</Text>
              </View>
            <View style={styles.input}>
            <Icon name="eyeo" size={30} color="black" />
              <TextInput value={this.state.pass} secureTextEntry={showPassword}  placeholder="NewPassword" style={{flex: 1}} onChangeText ={(typing)=>this.setState({pass:typing})} />
              <TouchableOpacity onPress={this.show}>
                <Icon name ="eye" size={30} color="black" />
              </TouchableOpacity>
            </View>
            <View style={styles.input}>
              <Icon name="eyeo" size={30} color="black" />
              <TextInput value={this.state.password} secureTextEntry={showPassword}  placeholder="ConfirmPassword" style={{flex: 1}} onChangeText ={(typing)=>this.setState({password:typing})} />
              <TouchableOpacity onPress={this.show}>
                <Icon name ="eye" size={30} color="black" />
              </TouchableOpacity>
            </View>
              <TouchableOpacity style={styles.botton} onPress={this.buttonPress}>
                <Text style={{color:'white',fontWeight:'bold',fontSize:20}}>Reset Password</Text>
              </TouchableOpacity>
              <View style={{flexDirection:'row',alignItems:'center'}}>
                  <TouchableOpacity>
                    <Text style={{color:'blue',fontWeight:'300',fontSize:20}}  title="Go To Login"
                        onPress={() => this.props.navigation.navigate('Login')}>-Back-</Text>
                </TouchableOpacity>
             </View>
          </View>
        )
      }
    }
    const styles = StyleSheet.create({
        container: {
          flex:1,
          alignItems: 'center',
          backgroundColor: "#F0FFFF",
          justifyContent:'center'
        },
        input:{
          flexDirection: 'row',
          alignItems: 'center',
          width: 300,
          borderWidth:3,
          backgroundColor:'white',
          borderRadius:20,
          marginTop:20,
          paddingHorizontal:8
        },
        logo:{
          marginTop:40,
          marginBottom:20,
          alignSelf:'center',
          width:200,
          height:150,
      },
        background:{
        flex: 1,
        resizeMode: 'cover',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        width: '100%',
        height: '100%',
      
      },
        botton:{
        marginTop: 30,
        borderWidth: 2,
        backgroundColor:'#6495ED',
        borderRadius:10,
        width:100,
        alignItems: 'center',
        elevation:10,
        width:300,
         
      }
      })