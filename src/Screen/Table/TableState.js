import React, { Component } from 'react'
import { Text, View ,StyleSheet, Button, TextInput,TouchableOpacity} from 'react-native'
import Icon from 'react-native-vector-icons/AntDesign';

export class TableState extends Component {
    constructor(){
        super()
        this.state = {
            data:[
                {id: 1,name:'Dio',address:'Cempaka'},
                {id: 2,name:'Cokorda',address:'Bekasi'},
                {id: 3,name:'Minzard',address:'Galaxy'},
            ],
            newID:'',
            newData:'',
            newAddress:'',
        }
    }

    tambah=()=>{
        const tampungan = this.state.data
        tampungan.push({id:this.state.newID,name:this.state.newData,address:this.state.newAddress})
        this.setState({
            data: tampungan,
            newID:'',
            newData:'',
            newAddress:''
        })
    }
    hapus=(gotdelete)=>{
        const dataBaru = this.state.data.filter(data => data.id != gotdelete.id)
        this.setState({
            data:dataBaru
        })
    }


    //     this.setState((prevState)=>({
    //         data:[...prevState.data, {id: 4,name:'Ariawan',address:'Cempaka'}]
    //     }))
    
    render() {
        const {showPassword} = this.state
        return (
            <View style={styles.container}>
                <View style={styles.table}>
                    <View style={styles.inTable}>
                        <Text style={styles.number}>#</Text>
                        <Text style={styles.name}>Name</Text>
                        <Text style={styles.address}>Address</Text>
                    </View>
                    {this.state.data.map((value,index)=>{
                        return(
                            <View key={index} style={styles.onTable}>
                                <Text style={[styles.number,styles.data]}>{index+1}</Text>
                                <Text style={[styles.name,styles.data]}>{value.name}</Text>
                                <Text style={[styles.address,styles.data]}>
                                    <TouchableOpacity onPress={()=>this.hapus(value)}>
                                        <Icon name = 'delete' size={30} color='black'/>
                                    </TouchableOpacity>
                                </Text>
                            </View>
                        )
                    })}
                </View>
            <View style={{backgroundColor:'#00BFFF',borderRadius:10,marginTop:20,alignItems:'center'}}>
                    <TextInput style={styles.input} placeholder='ID' onChangeText={(input)=>this.setState({newID : input})} value={this.state.newID}/>
                    <TextInput style={styles.input} placeholder='Nickname' onChangeText={(input)=>this.setState({newData : input})} value={this.state.newData}/>
                    <TextInput style={styles.input} placeholder='Address' onChangeText={(input)=>this.setState({newAddress : input})} value={this.state.newAddress}/>
                    <Button title='Tambah' onPress={this.tambah}/>    
            </View>
            </View>

        )
    }
}

export default TableState

const styles = StyleSheet.create({
    container:{
        backgroundColor:'white',
        flex:1,
        justifyContent:'center',
    },
    table:{
        backgroundColor:'#ADD8E6',
        width:'90%',
        marginHorizontal:20,
    },
    inTable:{
        flexDirection:'row',
        justifyContent:'space-around',
        backgroundColor:'#728FCE',
        borderWidth:2,
        height:60,
        alignItems:'center',
        borderRadius:5
    },
    onTable:{
        flexDirection:'row',
        justifyContent:'space-around',
        height:60,
        borderWidth:1,
        alignItems:'center',
        borderRadius:5,
    },
    name:{
        fontSize:20,
    },
    address:{
        fontSize:20,
    },
    number:{
        fontSize:20,
    },
    input:{
        borderWidth:2,
        width:'50%',
        marginHorizontal:20,
        marginTop:20,
        borderRadius:5,
        backgroundColor:'white',
        alignItems:'center',
        flexDirection:'row',
        
        
    },
})
