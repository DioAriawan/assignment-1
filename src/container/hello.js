import React, {Component} from 'react'
import {StyleSheet, Text, View, TextInput, TouchableOpacity, Image,value} from 'react-native';
import background from '../asset/images/background.jpg';

export default class StateExersice extends Component {
    constructor(){
        super()
        this.state = {
            name:'dio',
            input:'',
        }
    }
    buttonPress= ()=>{
        this.setState({
            name:this.state.input,
            input:'',
            
        })
    }
    render() {
        return (
            <View style={styles.container}>
                <Image style={styles.background} source={background}/>
                <Text style={{color:'white',fontSize:30}}>{JSON.stringify(this.state)}</Text>
                <View style={styles.kotak}>
                <Text style={{color:'white', fontWeight:'bold',fontSize:30,}}>{this.state.name}</Text>
                </View>
                <TextInput
                value={this.state.input}
                 style={styles.input}
                 placeholder= 'Username'
                 onChangeText ={(typing)=>this.setState({input:typing})}/>
                <TouchableOpacity style={styles.botton} onPress={this.buttonPress}>
                    <Text style={{color:'white',marginTop:10,fontSize:30}}>PressMe</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles =StyleSheet.create({
    container:{
        alignItems:'center',
        backgroundColor:'black',
        justifyContent:'center',
        flex:1,


    },
    kotak:{
        borderWidth:1,
        borderRadius:10,
        marginTop: 30,
        borderWidth: 2,
        backgroundColor:'black',
        borderRadius:10,
        width:100,
        alignItems: 'center',
        elevation:10
    },
    background:{
        flex: 1,
        resizeMode: 'cover',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        width: '100%',
        height: '100%',
    },
    botton:{
        borderWidth:1,
        borderRadius:10,
        marginTop: 30,
        borderWidth: 2,
        backgroundColor:'black',
        borderRadius:10,
        width:250,
        alignItems: 'center',
        elevation:10,
        
    },
    input:{
        width:300,
        borderWidth: 0.5,
        marginTop:20,
        backgroundColor:'white',
        fontWeight: 'bold',
        borderRadius:20,
        elevation:10
    }

})