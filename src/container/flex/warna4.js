import React, {Component} from 'react'
import { View } from 'react-native'

class StateExersice extends Component {
    render() {
        return (
            <View style={{flex:1}}>
                <View style={{flex:1, flexDirection:'row'}}>
                    <View style={{flex:1, backgroundColor:'cyan'}}/>
                    <View style={{flex:1, backgroundColor:'blue'}}/>
                </View>
                

                <View style={{flex:1, flexDirection:'row'}}>
                    <View style={{flex:1, backgroundColor:'yellow'}}/>
                    <View style={{width: 200, backgroundColor:'pink'}}/>
                    <View style={{flex:1}}>
                        <View style={{flex:1, backgroundColor:'cyan'}}/>
                        <View style={{flex:1, backgroundColor:'blue'}}/>
                    </View>
                </View>
                
                <View style={{flex:0.1, backgroundColor:'red'}}/>
                
                <View style={{flex:1,justifyContent:'space-between', flexDirection: 'row'}}>
                    <View style={{width:"20%"}}>
                        <View style={{flex:1,backgroundColor:'black'}}/>
                        <View style={{flex:1,backgroundColor:'red'}}/>
                    </View>
                    <View style={{width:"20%", backgroundColor:'blue'}}/>
                    <View style={{width:"20%"}}>
                        <View style={{flex:1,backgroundColor:'black'}}/>
                        <View style={{flex:1,backgroundColor:'red'}}/>
                    </View>    
                </View>
                <View style={{flex:1,flexDirection:'row',justifyContent:'center',padding:10,backgroundColor:'orange'}}></View>
            </View>
        )    
    }
}
export default StateExersice