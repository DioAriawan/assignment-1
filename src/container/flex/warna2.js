import React, {Component} from 'react'
import { View } from 'react-native'

class StateExersice extends Component {
    render() {
        return (
            <View style={{flex:1}}>


                <View style={{flex:1, flexDirection:'row'}}>
                    <View style={{flex:1, backgroundColor:'cyan'}}/>
                    <View style={{flex:1, backgroundColor:'blue'}}/>
                </View>
                <View style={{flex:1, flexDirection:'row'}}>
                    <View style={{flex:1, backgroundColor:'blue'}}/>
                    <View style={{flex:1, backgroundColor:'cyan'}}/>
                </View>
            </View>
        )
    }
}
export default StateExersice