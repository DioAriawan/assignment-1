import React, {Component} from 'react'
import { View } from 'react-native'

class StateExersice extends Component {
    render() {
        return (
            <View style={{flex:1}}>


                <View style={{flex:1, flexDirection:'row'}}>
                    <View style={{flex:1, backgroundColor:'cyan'}}/>
                    <View style={{flex:1, backgroundColor:'blue'}}/>
                </View>

                <View style={{flex:1, flexDirection:'row', justifyContent:'space-around'}}>
                    <View style={{width:'40%',backgroundColor:'red'}}/>
                    <View style={{width:'40%',backgroundColor:'red'}}/>
                </View>

                <View style={{flex:1, flexDirection:'row',}}>
                    <View style={{flex:1, backgroundColor:'yellow'}}/>
                    <View style={{flex:1, backgroundColor:'pink'}}/>
                    <View style={{flex:1}}>
                        <View style={{flex:1, backgroundColor:'cyan'}}/>
                        <View style={{flex:1, backgroundColor:'blue'}}/>
                    </View>
                </View>
            </View>
        )
    }
}
export default StateExersice