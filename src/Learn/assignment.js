import React, { Component } from 'react'
import { Text, View,Button,StyleSheet, TouchableOpacity,ScrollView,Image} from 'react-native'

export default class Assignment1 extends Component {
    constructor(){
        super()
        this.state = {
            data:[]
        }
    }
    // componentDidMount() {
    //     this.fetching()
    // }
    
    componentDidMount = () => {
        fetch('http://www.omdbapi.com/?s=avengers&apikey=997061b4')
        .then((response) => response.json())
        .then((json) => {
            this.setState({
                data:json.Search
            })
        })
        .catch((error) => {
            console.error(error);
        });
    }   
    // onPress = () => {
    //     alert('ariawan');
    
    // };
    render() {
        return (
            <View style={{flex:1}}>
                <View style={{backgroundColor: '#34282C', alignItems: 'center',height:80,justifyContent:'center'}}>
                        <Text style={{ color: 'white',fontSize:30 }}>Avenger Series</Text>
                    </View>
                <ScrollView>
                {this.state.data.map((value,index)=>{
                    return(
                    <View style={{ backgroundColor: '#34282C',}}key = {index}>
                        <View style={{backgroundColor:'#3B3131',width:350,height:200,borderRadius:20,padding:10,flexDirection:'row',justifyContent:'space-around',alignSelf:'center',marginVertical:15}}>
                            <Image style={{height:'100%',width:'35%',borderRadius:10,}}source={{uri:value.Poster}}/>
                                <View style={{flex:1,justifyContent:'space-between',padding:10}}>
                                    <Text style={{fontSize:25,color:'white'}}>{value.Title}</Text>
                                    <Text style={{fontSize:20,color:'white'}}>{value.Year}</Text>
                                    <Text style={{fontSize:20,color:'white'}}>{value.imdbID}</Text>
                                </View>
                        </View>
                    </View>
                    )
                })}
                </ScrollView>
            </View>
        )
    }
}
