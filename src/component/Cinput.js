import React, { Component } from 'react'
import { Text, View,StyleSheet,TextInput } from 'react-native'
import Icon from 'react-native-vector-icons/AntDesign';

export class Cinput extends Component {
    render() {
        return (
            <View style={styles.input}>
            <Icon name={this.props.name} size={30} color='black'/>
            <TextInput placeholder={this.props.placeholder}/>
        </View>
        )
    }
}
const styles = StyleSheet.create({
    input:{
        alignItems:'center',
        flexDirection:'row',
        paddingLeft: 25, 
        width:300,
        height: 50, 
        margin: 10, 
        borderWidth: 1,
        borderRadius:30,
        borderColor:'black',
        backgroundColor:'white',
        elevation: 15,
        paddingLeft:15,
    }
})
export default Cinput
