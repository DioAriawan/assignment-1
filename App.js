import  React,{Component} from 'react';
import {StyleSheet,Text,View,TextInput,TouchableOpacity,Image, } from 'react-native';
import SignUp from './signUp/sign up';
import Icon from 'react-native-vector-icons/FontAwesome5'
import Emoji from 'react-native-vector-icons/Entypo'
import Tampilan1 from './src/Tampilan/Tampilan1'
import Tampilan2 from './src/Tampilan/Tampilan2'
import Tampilan3 from './src/Tampilan/Tampilan3'
import Tampilan4 from './src/Tampilan/Tampilan4'
import Tampilan5 from './src/Tampilan/Tampilan5'
import LoginForm from './LoginForm/loginForm'
import loginForm from './LoginForm/loginForm';
import FilmCard from './src/FilmCard/FilmCard'
import Chat from './src/Chat/Chat'
import Assignment1 from './src/Learn/assignment';
import SplashScreen from './src/Screen/SplashScreen'
import TableState from './src/Screen/Table/TableState';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from './src/Screen/HomeScreen';
import DetailsScreen from './src/Screen/DetailsScreen'
import Hello from './src/container/hello'
import Forgot from './src/Screen/Forgot/'


const Stack = createStackNavigator();

const App: () => Node = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName='Splash' >
        <Stack.Screen name="Login" component={LoginForm} options={{headerShown:false}}/>
        <Stack.Screen name="SignUp" component={SignUp} options={{headerShown:false}}/>
        <Stack.Screen name= "Forgot" component={Forgot} options={{headerShown:false}}/>
        <Stack.Screen name="Splash" component={SplashScreen} options={{headerShown:false}}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
