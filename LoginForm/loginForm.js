import  React,{Component} from 'react';
import {StyleSheet,Text,View,TextInput,TouchableOpacity,Image,Button } from 'react-native';
import background from '../src/asset/images/background.jpg';
import logo from '../src/asset/images/foto.png';
import Icon from 'react-native-vector-icons/AntDesign';

export default class loginForm extends Component{
  constructor() {
    super();
    this.state = {
      showPassword:true}
}
show=()=>{
  this.setState({
    showPassword: !this.state.showPassword
  })

}
buttonPress= ()=>{
  this.setState({
      email:'',
      password:'',
      
  })
}



  render() {
    const {showPassword} = this.state
    return (
      <View style={styles.container}>
        <Image style={styles.background} source={background}/>
        <Image style={styles.logo} source={logo}/>
        <View style={styles.input}>
          <Icon name='user' size={30} color='black'/>
          <TextInput value={this.state.email} placeholder="Username/Email"  onChangeText ={(typing)=>this.setState({email:typing})}/>
        </View>
        <View style={styles.input}>
          <Icon name="eyeo" size={30} color="black" />
          <TextInput value={this.state.password} secureTextEntry={showPassword}  placeholder="Password" style={{flex: 1}} onChangeText ={(typing)=>this.setState({password:typing})} />
          <TouchableOpacity onPress={this.show}>
            <Icon name ="eye" size={30} color="black" />
          </TouchableOpacity>
        </View>
        <TouchableOpacity style={{marginTop: 15}} >
          <Text style={{color:'black',fontSize:15,fontWeight:'bold'}} title="Go To Forgot" onPress={() => this.props.navigation.navigate('Forgot')}>Forgot Password ?</Text>
        </TouchableOpacity>
          <TouchableOpacity style={styles.botton} onPress={this.buttonPress}>
            <Text style={{color:'white',fontWeight:'bold',fontSize:20}}>Login</Text>
          </TouchableOpacity>
          <View style={{flexDirection:'row',alignItems:'center'}}>
            <Text style={{color:'black',fontWeight:'bold',fontSize:20}}>Not A Member?</Text>
              <TouchableOpacity>
                <Text style={{color:'blue',fontWeight:'300',fontSize:20}}  title="Go To Sign Up"
                    onPress={() => this.props.navigation.navigate('SignUp')}>SignUp Now</Text>
            </TouchableOpacity>
         </View>
          

      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex:1,
    justifyContent:'center',
    alignItems: 'center',
    backgroundColor: "#F0FFFF"
  },
  input:{
    flexDirection: 'row',
    alignItems: 'center',
    width: 300,
    borderWidth:3,
    backgroundColor:'white',
    borderRadius:20,
    marginTop:20,
    paddingHorizontal:8
  },
  logo:{
    marginTop:40,
    marginBottom:20,
    alignSelf:'center',
    width:200,
    height:150,
},
  background:{
  flex: 1,
  resizeMode: 'cover',
  justifyContent: 'center',
  alignItems: 'center',
  position: 'absolute',
  width: '100%',
  height: '100%',

},
  botton:{
  marginTop: 30,
  borderWidth: 2,
  backgroundColor:'#6495ED',
  borderRadius:10,
  width:100,
  alignItems: 'center',
  elevation:10,
  width:300,
  
}
})